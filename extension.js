/*
    Gnome Shell Extension: Deja Dup Tray
    Author: Cesar A. Gaspar
    Email: rasec.rapsag@gmail.com
    Site: https://gitlab.com/rasec.rapsag/dejadup-tray
    License: MIT
*/
'use strict';

const { GObject, St, Gio } = imports.gi;
const ExtensionUtils = imports.misc.extensionUtils;
const Main = imports.ui.main;
const Util = imports.misc.util;
const Mainloop = imports.mainloop;
const Me = ExtensionUtils.getCurrentExtension();
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;

const _ = ExtensionUtils.gettext;

let timeout;

const Indicator = GObject.registerClass(
class Indicator extends PanelMenu.Button {
    _init() {
        super._init(0.0, _('Deja Dup Tray'));

        this._icon = new St.Icon({
            icon_name: 'emblem-synchronizing-symbolic',
            style_class: 'system-status-icon',
        });
        this.add_child(this._icon);

        /* Last backup item */
        const drive = Gio.Settings.new('org.gnome.DejaDup.Drive');
        let backup_name = drive.get_string('name');
        let backup_msg;
        if (backup_name) {
            backup_msg = _('Last Backup on') + ` "${backup_name}":`
        } else {
            backup_msg = _('No backup configured')
        }
        let item_drive = new PopupMenu.PopupMenuItem(backup_msg);
        item_drive.sensitive = false;
        this.menu.addMenuItem(item_drive);

        if (backup_name) {
            const dejadup = Gio.Settings.new('org.gnome.DejaDup');
            let last_backup = new Date(dejadup.get_string('last-backup'));

            const options = {
                year: "numeric",
                month: "long",
                day: "numeric",
            };

            let item_backup = new PopupMenu.PopupMenuItem(
                last_backup.toLocaleString(undefined, options)
            );
            item_backup.sensitive = false;
            this.menu.addMenuItem(item_backup);

            /* Verify backup */
            timeout = Mainloop.timeout_add_seconds(60, () => {

                last_backup = new Date(dejadup.get_string('last-backup'));

                const TotalDays = (date_1, date_2) => {
                    let difference = date_1.getTime() - date_2.getTime();
                    return Math.floor(difference / (1000 * 3600 * 24));
                }

                // Update icon
                if (TotalDays(new Date(), last_backup) > 6) {
                    this._icon.add_style_class_name('backup-alert');
                } else {
                    this._icon.remove_style_class_name('backup-alert');
                }

                item_backup.label.text = last_backup.toLocaleString(undefined, options);

                log('Update...');
                return true;
            });
            /* Verify backup */
        }

        this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());

        /* Backup now */
        let item_backup_now = new PopupMenu.PopupMenuItem(_('Backup Up Now'));
        item_backup_now.connect('button-press-event', () => {
            Util.spawn(['/usr/bin/deja-dup', '--backup']);
        });
        this.menu.addMenuItem(item_backup_now)

        this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());

        /* Open Deja Dup */
        let item_dejadup = new PopupMenu.PopupMenuItem(_('Open Déjà Dup Backups'));
        item_dejadup.connect('button-press-event', () => {
            Util.spawn(['/usr/bin/deja-dup']);
        });
        this.menu.addMenuItem(item_dejadup);
    }
});

class Extension {
    constructor(uuid) {
        this._uuid = uuid;

        ExtensionUtils.initTranslations(Me.metadata['gettext-domain']);
    }

    enable() {
        this._indicator = new Indicator();
        Main.panel.addToStatusArea(this._uuid, this._indicator);
    }

    disable() {
        Mainloop.source_remove(timeout);
        this._indicator.destroy();
        this._indicator = null;
    }
}

function init(meta) {
    return new Extension(meta.uuid);
}
