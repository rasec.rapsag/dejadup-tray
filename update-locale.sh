#!/usr/bin/env zsh

POT_FILE='dejadup-tray@rasec.rapsag.pot'
PO_REVISION_DATE=$(date '+%Y-%m-%d %H:%M%z')

touch "${POT_FILE}"

xgettext -j --from-code=UTF-8 --no-wrap --output="${POT_FILE}" *.js

for lang in locale/*.po; do
    PO_FILE="${lang}"

    echo -n "${PO_FILE}"
    POT_CREATE_DATE=$(sed -n 's/\(^"POT-.*e: \)\(.*\)\(\\.*\)/\2/p' "${PO_FILE}")
    msgmerge --update --backup=off "${PO_FILE}" "${POT_FILE}"

    # Replace dates
    sed -i 's/\(^"POT-.*e: \)\(.*\)\(\\.*\)/\1'${POT_CREATE_DATE}'\\n"/g' ${PO_FILE}
    sed -i 's/\(^"PO-.*e: \)\(.*\)\(\\.*\)/\1'${PO_REVISION_DATE}'\\n"/g' ${PO_FILE}
done

rm "${POT_FILE}"
