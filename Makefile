all: build

.PHONY: build install dist

build:
	./update-locale.sh

dist: build
	rm -f dejadup-tray*.zip
	gnome-extensions pack --podir=locale .

install: dist
	gnome-extensions install dejadup-tray@rasec.rapsag.shell-extension.zip
